
import './admin.css';
import { NavLink as Link, } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import React, {useState,useEffect } from 'react';
import axios from 'axios';
import { auth,db } from '../Firebase';
import { Button,Modal } from 'react-bootstrap';
import moment from "moment";
const Contact= () => {

    const [annonce,setAnnonce]=useState([]);
    const [search,setSearch]=useState("");
   
  
    
    const [nom,setNom]=useState("");
    const [prenom,setPrenom]=useState("");
    const [email,setEmail]=useState("");
    const [telephone,setTelephone]=useState("");
    const [Password,setPassword]=useState("Azektjg122");
    const [image,setImage]=useState("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS54088iJjHpn-y9FCxGAh5NBEdHugwIXewWQ&usqp=CAU");
    


    const [nomU,setnomU]=useState("");
    const [prenomU,setprenomU]=useState("");
    const [emailU,setemailU]=useState("");
    const [telephoneU,settelephoneU]=useState("");
    const [imageU,setimageU]=useState("");
    const [PasswordU,setPasswordU]=useState("");
    const [idUser,setidUser]=useState("");
const onAnnonce= async ()=>{
  
    
    await   db.collection("users")
      .add({
               
               nom,
               prenom,
               email,
               telephone,
              image,
              Password
           })
           
           .then((res)=> {     
            })  
           .catch((error)=>
           console.log(error)
           ); 
           
   };
   


  
    

   const getAnnonce= () =>{
       try {
        db.collection("users").onSnapshot((querySnapshot) => {
          const Trajet = [];
          querySnapshot.forEach((doc) => {
            const { nom, prenom, email,telephone,image,Password } = doc.data();
            Trajet.push({  
              id: doc.id,
              nom, prenom, email,telephone,image,Password
            });
          });
          setAnnonce(Trajet);
         // setDate(Trajet.date_depart);
         // setIsLoading(false);
        });
       } catch (error) {
          console.log(error); 
       }
       
      };

 
 

  useEffect(()=>{

    getAnnonce();
    
  },[]);


  const handDelete = async (id)=> {
        
       
    await db.collection("users").doc(id).delete().then(() => {
        alert("Document successfully deleted!");
        getAnnonce();
    }).catch((error) => {
        console.error("Error removing document: ", error);
    });
   
   
  };


  const [show, setShow] = useState(false);

  const handleClose = () =>{
     
       setShow(false);

  };
  const handleShow = (data) =>{
   


    setShow(true);
    setidUser(data.id);
    setnomU(data.nom);
    setprenomU(data.prenom);
    setemailU(data.email);
    settelephoneU(data.telephone);
    setimageU(data.image);
    setPasswordU(data.Password);
  };

  const handleUpdate = async () =>{
   
      
    await db.collection("users").doc(idUser).set({
      
        nom:nomU,
        prenom:prenomU,
        email:emailU,
        telephone:telephoneU,
       image:imageU,
       Password:PasswordU,
    }).then(() => {
        handleClose();
        alert("Document successfully Update!");
        getAnnonce();
    }).catch((error) => {
        console.error("Error  document: ", error);
    });

  };


  const tab= annonce.map((val,i)=>{
    return(
     
    <tr key={i} >
       <td className="text-bold">{val.nom}  {val.prenom}</td>
  <td className="text-bold">{val.email}</td>
  <td className="text-bold">{val.telephone}</td>
 
  <td  className=" ml-4">
  <button className="btn btn-danger"  onClick={()=>handDelete(val.id)}><i className="fa fa-trash-o text-gray" aria-hidden="true"></i> Delete</button>
 
  <Button variant="warning ml-3"  /*className="mt-3 text-bold"*/ onClick={()=>{handleShow(val)}}>
         Edit
      </Button>

     
      <Modal show={show} onHide={handleClose}>
    <Modal.Header closeButton>
      <div  className="ml-5">
    <Modal.Title > Update User</Modal.Title>
    </div>
    </Modal.Header>
    <Modal.Body>
   

    <div className="form">   
        <form action="" method="post" className="form-horizontal">
       
            <div className="form-group">
            <label  className="control-label col-sm-6" htmlFor="">Nom</label>
            <div className="col-sm-10">
            <input className="form-control"type="text" placeholder="Entrez le Nom !" value={nomU} onChange={(e)=>{setnomU(e.target.value);}}/>
            </div>
            </div>

            
            <div className="form-group">
            <label  className="control-label col-sm-6" htmlFor="">Prenom</label>
            <div className="col-sm-10">
            <input className="form-control"type="text" placeholder="Entrez le prenom !" value={prenomU} onChange={(e)=>{setprenomU(e.target.value);}}/>
            </div>
            </div>

            
            <div className="form-group">
            <label  className="control-label col-sm-6" htmlFor="">Email</label>
            <div className="col-sm-10">
            <input className="form-control"type="email" placeholder="Entrez l'adresse Email !" value={emailU} onChange={(e)=>{setemailU(e.target.value);}}/>
            </div>
            </div>
            
            <div className="form-group">
            <label  className="control-label col-sm-6" htmlFor="">Telephone</label>
            <div className="col-sm-10">
            <input className="form-control"type="tel" placeholder="Entrez le Numero de Telephone !" value={telephoneU} onChange={(e)=>{settelephoneU(e.target.value);}}/>
            </div>
            </div>    
           
     </form>
    </div>


    </Modal.Body>
    <Modal.Footer>
      <Button variant="secondary" onClick={handleClose}>
        Cancel
      </Button>
      <Button variant="primary" onClick={handleUpdate}>
        Valider
      </Button>
    </Modal.Footer>
  </Modal>
    </td>
    </tr>
    )
   });

    return (

        
        <div>
            <div className="row">
                <div className="col-md-2">
       <div className="sidebarse">

          <h4 className="border-bottom mt-3 p-1"><i className="fa fa-tachometer mr-2" aria-hidden="true"></i>Dashboard</h4>
    
       <Link to="admin" className="border-bottom mt-3"><i className="fa fa-cart-arrow-down mr-2" aria-hidden="true"></i>Annonce</Link>
       <Link to="contact" className="border-bottom mt-3"><i className="fa fa-user  mr-2" aria-hidden="true"></i> Users</Link>
       <Link to="trajet" className="border-bottom mt-3"><i className="fa fa-fw fa-envelope  mr-2">Trajet</i></Link>
       
       <Link to="/admin" className="border-bottom mt-3"><i className="fa fa-fw fa-home  mr-2"></i> Charts</Link>
       <Link to="/admin" className="border-bottom mt-3"><i className="fa fa-shopping-cart  mr-2" aria-hidden="true"></i> Setting</Link>
       <Link to="/admin" className="border-bottom mt-3"><i className="fa fa-building  mr-2" aria-hidden="true"></i> Pages</Link>
      
       <Link to="/admin" className="border-bottom mt-3"><i className="fa fa-bars  mr-2" aria-hidden="true"></i> Autres</Link>
       
         </div>
    </div>
    <div className="col-md-10">
    <div className="main">
    
  
    <Button variant="primary"  className="mt-3 text-bold" onClick={handleShow}>
        Ajouter User
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <div  className="ml-5">
        <Modal.Title > Add User</Modal.Title>
        </div>
        </Modal.Header>
        <Modal.Body>
       

        <div className="form">   
            <form action="" method="post" className="form-horizontal">
           
                <div className="form-group">
                <label  className="control-label col-sm-6" htmlFor="">Nom</label>
                <div className="col-sm-10">
                <input className="form-control"type="text" placeholder="Entrez le Nom !" onChange={(e)=>{setNom(e.target.value);}}/>
                </div>
                </div>

                
                <div className="form-group">
                <label  className="control-label col-sm-6" htmlFor="">Prenom</label>
                <div className="col-sm-10">
                <input className="form-control"type="text" placeholder="Entrez le prenom !" onChange={(e)=>{setPrenom(e.target.value);}}/>
                </div>
                </div>

                
                <div className="form-group">
                <label  className="control-label col-sm-6" htmlFor="">Email</label>
                <div className="col-sm-10">
                <input className="form-control"type="email" placeholder="Entrez l'adresse Email !" onChange={(e)=>{setEmail(e.target.value);}}/>
                </div>
                </div>
                
                <div className="form-group">
                <label  className="control-label col-sm-6" htmlFor="">Telephone</label>
                <div className="col-sm-10">
                <input className="form-control"type="tel" placeholder="Entrez le Numero de Telephone !" onChange={(e)=>{setTelephone(e.target.value);}}/>
                </div>
                </div>

              
                
               
         </form>
        </div>




        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Cancel
          </Button>
          <Button variant="primary" onClick={onAnnonce}>
            Valider
          </Button>
        </Modal.Footer>
      </Modal>







  <h2>Liste des Users</h2>

  
  <table className="table table-striped text-bold">
    <thead>
    <tr>
      
      <th>Utilisateur</th>
      <th>Email</th>
      <th>Telephone</th>
      <th></th>
      </tr>
    </thead>
   <tbody>

    {tab}
      </tbody>
      </table>
     </div>
      </div>

        </div>





        </div>
    );
};

export default Contact;