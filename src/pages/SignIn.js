import React,{useState} from 'react';
import axios from 'axios';
import Navigation from '../composant/Navigation';
import 'bootstrap/dist/css/bootstrap.min.css';
const SignIn = () => {


  
  const [email,setEmail]=useState("");
  const [password,setPassword]=useState("");

  const [errorE,setErrorE]=useState("");
  const [errorP,setErrorP]=useState("");
  
  const handSubmit=(e)=>{

      e.preventDefault();
      
             
       const dat={
             
              email:email,
              password:password
             
           };
           axios(
             {
               method:'post',
               url:"http://localhost:5000/api/login", 
           data:dat
           
           
             })
               .then((res) =>{
                   console.log(res)

                   if(res.data.errors){

                     
                      setErrorE(res.data.errors.email);
                      setErrorP(res.data.errors.password);
                           }
                           else{
                              

                              window.location="/";
                           }
                          }
                      
               )
               
               .catch(e => console.log(e));
      
              
              };
    return (
  <div>
            <Navigation/>

            <div className="container"> 
   <h2>connexion</h2>
      <form className="form-horizontal" onSubmit={handSubmit} >

    
      <div className="form-group">
      <label className="control-label col-sm-2" htmlFor="email">Email:</label>
      <div className="col-sm-10">
        <input type="email" className="form-control" id="em" placeholder="Entrer email" name="email" 
        onChange={(e)=>{setEmail(e.target.value);}} />
      </div>
      <div className="errorEmail text-success"> {errorE}</div>
    </div>
    
    <div className="form-group">
      <label className="control-label col-sm-2" htmlFor="pwd">Password:</label>
      <div className="col-sm-10">          
        <input type="password" className="form-control" id="pwd" placeholder="Entrer password" name="pwd"  
         onChange={(e)=>{setPassword(e.target.value);}}
        /> 
      </div>
      <div className="errorPass text-success">{errorP}</div>
    </div>
    
    

    <div className="form-group">        
      <div className="col-sm-offset-2 col-sm-10">
        <input type="submit" className="btn btn-success"  placeholder="Connecter"/>
      </div>
    </div>
  </form>
  </div>











        </div>
    );
};

export default SignIn;