
import React, {useState,useEffect } from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import { NavLink as Link, } from 'react-router-dom';
//import Edit from './Edit';
//import List from './List';
import Navigation from '../composant/Navigation';
import Footer from '../composant/Footer';

import Sidebar from './Sidebar.js';
import './service.css';


const Vehicule = () => {
    
    const [annonce,setAnnonce]=useState([]);
    const [search,setSearch]=useState("");
   
    
   const getAnnonce= async () =>{
       try {
        const res=  await axios.get("http://localhost:5000/api/annonce/telephone");
        setAnnonce(res.data);
        console.log(res);
       } catch (error) {
          console.log(error); 
       }
       
      };

      /* const handDelete = async (id)=> {
        
         const res= await axios.delete("http://localhost:5000/api/annonce/"+id).catch(e=>console.log(e));
         
         if(res){
             getAnnonce();
         }
        
       };*/
 
 

  useEffect(()=>{

    getAnnonce();
    
  },[]);
    return (
        <>
         <Navigation/> 
  

    
     
        <h1>  </h1>
      
<div className="container">
<div className="row">
 <div className="col-md-10 m-3">
 <div className="row justify-content-center">
                     <div className="col-12 col-md-10 col-lg-8">
                         <form className="card card-sm">
                             <div className="card-body row no-gutters align-items-center">
                                 <div className="col-auto">
                                     <i className="fas fa-search h4 text-body"></i>
                                 </div>
                               
                                 <div className="col">
                                     <input className="form-control form-control-lg form-control-borderless" type="search" placeholder="Recherche par categorie" 
                                      onChange={(e)=>{setSearch(e.target.value);}}
                                     />
                                 </div>
                                 
                                 
                                 <div className="col-auto">
                                     <button className="btn btn-lg btn-success" type="submit">Search</button>
                                 </div>
                                 
                                 
                             </div>
                         </form>
                         
                     </div>
                   
                 </div>
                 <Link to="/signup" className="btn btn-lg btn-success font-bold">Créer annonce</Link>
</div>
</div>
<div className="row mt-4">
 <div className="col-md-4">

 <Sidebar/>

<div className="main">

</div>







 </div>
 <div className="col-md-8">
     <div className="d-flex  justify-content-between align-items-center flex-wrap m-2">   


{ annonce.filter((val)=>{
    if(search==""){
       return val;
    }else if(val.title.toLowerCase().includes(search.toLowerCase()) ){
        return val;
    }else if(val.category.toLowerCase().includes(search.toLowerCase()) ){
     return val;
      }
      else if(val.ville.toLowerCase().includes(search.toLowerCase()) ){
         return val;
          }

}).map((val)=>{
     return(
    <>


 <Link to={`/service/${val._id}`}>  


 <div className="card d-flex mb-4" style={{width:"18rem",boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)"}}>
<img className="card-img-top" src={val.picture} alt="Card image cap" width="100px" height="150px"/>
<div className="card-body text-center">
 <h5 className="card-title">{val.title}</h5>
 <p className="card-text">{val.prix}TN</p>
 <a href="#" className="btn btn-primary">Voir</a>
</div>
</div>   

</Link> 

   
</>
)
})}
              </div>
      </div>
    </div>


    <nav aria-label="...">
  <ul className="pagination">
 <li className="page-item disabled">
   <span className="page-link">Previous</span>
 </li>
 <li className="page-item"><a className="page-link" href="#">1</a></li>
 <li className="page-item active">
   <span className="page-link">
     2
     <span className="sr-only">(current)</span>
   </span>
 </li>
 <li className="page-item"><a className="page-link" href="#">3</a></li>
 <li className="page-item">
   <a className="page-link" href="#">Next</a>
 </li>
</ul>
</nav>









  </div>

  <Footer/>
</>
    )
};

export default Vehicule;