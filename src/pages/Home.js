import React from 'react';
import Navigation from '../composant/Navigation';
import Footer from '../composant/Footer';
import Newform from './Newform';
const Home = () => {
  

    
    return (
        <div>
            <Navigation/>
            
              <Newform/>
              <Footer/>
        </div>
    );
};

export default Home;