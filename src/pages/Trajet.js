
import './admin.css';
import { NavLink as Link, } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import React, {useState,useEffect } from 'react';
import { auth,db } from '../Firebase';
import { Button,Modal } from 'react-bootstrap';
import moment from "moment";
const Trajet= () => {

    const [annonce,setAnnonce]=useState([]);
    const [search,setSearch]=useState("");
   
  
  
    

  
  
    const [paysDep,setPaysDep]=useState("Allemagne");
    const [paysAr,setPaysAr]=useState("Allemagne");
    const [date,setDate]=useState(moment());
    const [dateAr,setDateAr]=useState(moment());

    const [paysDepU,setPaysDepU]=useState("");
    const [paysArU,setPaysArU]=useState("");
    const [dateU,setDateU]=useState("");
    const [dateArU,setDateArU]=useState("");
    const [idTrajet,setIdtrajet]=useState("");
    const [Iduser,setIduser]=useState("");
    const onAnnonce= async (e)=>{
  
        e.preventDefault();
    
        await   db.collection("trajets")
          .add({
                   Iduser:"1522", 
                   date_depart:date,
                   date_arrivee:dateAr,
                   ville_depart:paysDep,
                   ville_arrivee:paysAr,
                  
               })
               
               .then((res)=> {  
                 
                handleClose();
                 //navigation.navigate('ListAnnonce');
                })  
               .catch((error)=>
               console.log(error)
               ); 
               
               // 
               
    };







   const getAnnonce= () =>{
       try {
        db.collection("trajets").onSnapshot((querySnapshot) => {
          const Trajet = [];
          querySnapshot.forEach((doc) => {
            const {date_depart,  date_arrivee, ville_depart,ville_arrivee,Iduser } = doc.data();
            Trajet.push({  
              id: doc.id,
              date_depart,  date_arrivee, ville_depart,ville_arrivee,Iduser
            });
          });
          setAnnonce(Trajet);
         // setDate(Trajet.date_depart);
         // setIsLoading(false);
        });
       } catch (error) {
          console.log(error); 
       }
       
      };

 
 

  useEffect(()=>{

    getAnnonce();
    
  },[]);


  const handDelete = async (id)=> {
       
    await db.collection("trajets").doc(id).delete().then(() => {
        alert("Document successfully deleted!");
        getAnnonce();
    }).catch((error) => {
        console.error("Error removing document: ", error);
    });
   
  };


  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
 
  const handleShow = (data) =>{
   
    setShow(true);
    setDateU(data.date_depart);
    setDateArU(data.date_arrivee);
    setPaysDepU(data.ville_depart);
    setPaysArU(data.ville_arrivee);
    setIdtrajet(data.id);
   setIduser(data.Iduser);
  };

  const handleUpdate = async () =>{
   
      
    await db.collection("trajets").doc(idTrajet).set({
      
                   Iduser:Iduser, 
                   date_depart:dateU,
                   date_arrivee:dateArU,
                   ville_depart:paysDepU,
                   ville_arrivee:paysArU,
    }).then(() => {
        handleClose();
        alert("Document successfully Update!");
        getAnnonce();
    }).catch((error) => {
        console.error("Error  document: ", error);
    });

  };




  const tab= annonce.map((val,i)=>{
    return(
     
    <tr key={i} >
      
  <td className="text-bold">{val.ville_depart}</td>
  <td className="text-bold">{val.ville_arrivee}</td>
  <td className="text-bold">{val.date_depart}</td>
  <td className="text-bold">{val.date_arrivee}</td>
  <td  className=" ml-4">
  <button className="btn btn-danger"  onClick={()=>handDelete(val.id)}><i className="fa fa-trash-o text-gray" aria-hidden="true"></i> Delete</button>
 
 
  <Button variant="warning ml-3"  onClick={()=>{handleShow(val)}}>
        Edit
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <div  className="ml-5">
        <Modal.Title > Update Trajet</Modal.Title>
        </div>
        </Modal.Header>
        <Modal.Body>
       

        <div className="form">   
            <form action="" method="post" className="form-horizontal">
           
               

                <div class="form-group">
                 <label for="exampleFormControlSelect1">Pays de depart</label>
                  <select class="form-control" placeholder=" select" value={paysDepU} id="exampleFormControlSelect1"
                   onChange={(e)=>{setPaysDepU(e.target.value);}}
                  >
                
                <option value="Allemangne">Allemagne</option>
                  <option  value="France"> France</option>
                  <option  value="Togo"> Togo</option>
                  <option  value="Tunisie"> Tunisie</option>
                  <option value="Zambie">Zambie</option>
                   </select>
                </div>

                 
                <div class="form-group">
                 <label for="exampleFormControlSelect1">Pays d'arrivé</label>
                  <select class="form-control"  id="exampleFormControlSelect"  value={paysArU}
                   onChange={(e)=>{setPaysArU(e.target.value);}}
                  >
                  <option value="Allemangne">Allemagne</option>
                  <option  value="France"> France</option>
                  <option  value="Togo"> Togo</option>
                  <option  value="Tunisie"> Tunisie</option>
                  <option value="Zambie">Zambie</option>
                  
                
                   </select>
                </div>

                



                <div className="form-group">
                <label className="control-label col-sm-6"  htmlFor=""> Date de depart </label>
                <div className="col-sm-10">
                <input className="form-control " type="date" onChange={(e)=>{setDateU(e.target.value);}} value={dateU}/>
                 </div>
                </div>


                <div className="form-group">
                <label className="control-label col-sm-6"  htmlFor=""> Date d'arrivé </label>
                <div className="col-sm-10">
                <input className="form-control " type="date" onChange={(e)=>{setDateArU(e.target.value);}}  value={dateArU}/>
                 </div>
                </div>

               
         </form>
        </div>




        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Cancel
          </Button>
          <Button variant="primary" onClick={handleUpdate}>
            Valider
          </Button>
        </Modal.Footer>
      </Modal>



    </td>
    </tr>
    )
   });

    return (

        
        <div>
            <div className="row">
                <div className="col-md-2">
       <div className="sidebarse">

          <h4 className="border-bottom mt-3 p-1"><i className="fa fa-tachometer mr-2" aria-hidden="true"></i>Dashboard</h4>
    
       <Link to="admin" className="border-bottom mt-3"><i className="fa fa-cart-arrow-down mr-2" aria-hidden="true"></i>Annonce</Link>
       <Link to="contact" className="border-bottom mt-3"><i className="fa fa-user  mr-2" aria-hidden="true"></i> Users</Link>
       <Link to="trajet" className="border-bottom mt-3"><i className="fa fa-fw fa-envelope  mr-2">Trajet</i></Link>
       
       <Link to="/admin" className="border-bottom mt-3"><i className="fa fa-fw fa-home  mr-2"></i> Charts</Link>
       <Link to="/admin" className="border-bottom mt-3"><i className="fa fa-shopping-cart  mr-2" aria-hidden="true"></i> Setting</Link>
       <Link to="/admin" className="border-bottom mt-3"><i className="fa fa-building  mr-2" aria-hidden="true"></i> Pages</Link>
      
       <Link to="/admin" className="border-bottom mt-3"><i className="fa fa-bars  mr-2" aria-hidden="true"></i> Autres</Link>
       
         </div>
    </div>
    <div className="col-md-10">
    <div className="main">
    
  
      <Button variant="primary"  className="mt-3 text-bold" onClick={handleShow}>
        Ajouter Trajet
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <div  className="ml-5">
        <Modal.Title > Add Trajet</Modal.Title>
        </div>
        </Modal.Header>
        <Modal.Body>
       

        <div className="form">   
            <form action="" method="post" className="form-horizontal">
           
               

                <div class="form-group">
                 <label for="exampleFormControlSelect1">Pays de depart</label>
                  <select class="form-control" placeholder=" select"
                   onChange={(e)=>{setPaysDep(e.target.value);}}
                  >
                
                <option value="Allemangne">Allemagne</option>
                  <option  value="France"> France</option>
                  <option  value="Togo"> Togo</option>
                   <option value="Tunisie"> Tunisie</option>
                   <option value="Zambie">Zambie</option>
                  
                   </select>
                </div>

                 
                 <div class="form-group">
                 <label for="exam">Pays d'arrivé</label>
                  <select class="form-control" 
                   onChange={(e)=>{setPaysAr(e.target.value);}}
                  >
                  <option value="Allemangne">Allemagne</option>
                  <option  value="France"> France</option>
                  <option  value="Togo"> Togo</option>
                  <option  value="Tunisie"> Tunisie</option>
                   <option  value="Zambie">Zambie</option>
                  
                
                   </select>
                </div>

                



                <div className="form-group">
                <label className="control-label col-sm-6"  htmlFor=""> Date de depart </label>
                <div className="col-sm-10">
                <input className="form-control " type="date" onChange={(e)=>{setDate(e.target.value);}}/>
                 </div>
                </div>


                <div className="form-group">
                <label className="control-label col-sm-6"  htmlFor=""> Date d'arrivé </label>
                <div className="col-sm-10">
                <input className="form-control " type="date" onChange={(e)=>{setDateAr(e.target.value);}}/>
                 </div>
                </div>

               
         </form>
        </div>




        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Cancel
          </Button>
          <Button variant="primary" onClick={onAnnonce}>
            Valider
          </Button>
        </Modal.Footer>
      </Modal>







  <h2>Liste des Trajets</h2>

  
  <table className="table table-striped text-bold">
    <thead>
    <tr>
      
      <th>Pays de depart</th>
      <th>Pays d'arrivé</th>
      <th>Date de deart</th>
      <th>Date d'arrivé</th>
      <th></th>
      </tr>
    </thead>
   <tbody>

    {tab}
      </tbody>
      </table>
     </div>
      </div>

        </div>



        </div>
    );
};

export default Trajet;
 //<Button variant="primary"  className="mt-3 text-bold" onClick={handleShow}>
 // <i className="fa fa-trash-o text-gray" aria-hidden="true"></i>
   //     Edit
    //  </Button>
