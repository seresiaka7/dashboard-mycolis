import React, {useState,useEffect } from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import Navigation from '../composant/Navigation';
import Footer from '../composant/Footer';
function Detail  ({match}){
   
    const [annonce,setAnnonce]=useState([])
   
    useEffect(()=>{
getAnnonce();
        console.log(match);
        
      },[]);
    const getAnnonce= async () =>{
        try {
         const res=  await axios.post(`http://localhost:5000/api/annonce/${match.params.id}`);
         setAnnonce(res.data);
         console.log(res);
        } catch (error) {
           console.log(error); 
        }
       };
   return (
        <div>
 <Navigation/> 
          
<div className="container mb-4 " style={{marginTop:"18rem"}}>
    <div className="row">
        <div className="col-md-6">

 <div className="" >
  <img
    src={annonce.picture}
    className=""
    alt="..."
    width="560px" height="500px"
  />
  <div className="">
       <p className="">
         <h1>Description</h1>
      {annonce.description}
       </p>
     </div>
  </div>
  </div>

<div className="col-md-6">
<div className="right">


  <h1 className="prix">
    {annonce.prix} DT
  </h1>

  
  <h2 className="titre">
    {annonce.title}
  </h2>
  <p>publié  le 25 décembre 2021 </p>
  <p className="border-bottom"><i className="fa fa-tags mr-2" aria-hidden="true"></i> {annonce.ville}</p>
  <p className="border-bottom"><i className="fa fa-tags mr-2" aria-hidden="true" ></i> {annonce.category}</p>
  
</div>


</div>




  </div>


  
</div >


<div >
          <Footer/>

</div>
        </div>
    )
};

export default Detail;