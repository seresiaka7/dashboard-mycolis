import React from 'react';
import { NavLink as Link, } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
const Sidebar = () => {
    return (
        <div>
         <div className="sidebar ">
       <Link  to="/vehicule" className="border-bottom"><i className="fa fa-car mr-2" aria-hidden="true"></i>Vehicules</Link>
       <Link to="services" className="border-bottom"><i className="fa fa-fw fa-wrench  mr-2"></i> Services</Link>
       <Link to="clients" className="border-bottom"><i className="fa fa-laptop  mr-2" aria-hidden="true"></i> Informatique</Link>
       <Link to="/" className="border-bottom"><i className="fa fa-fw fa-envelope  mr-2"></i>  jardin</Link>
       <Link to="/" className="border-bottom"><i className="fa fa-mobile  mr-2" aria-hidden="true"></i> Telephone</Link>
       <Link to="/" className="border-bottom"><i className="fa fa-fw fa-home  mr-2"></i> Immobilier</Link>
       <Link to="/" className="border-bottom"><i className="fa fa-shopping-cart  mr-2" aria-hidden="true"></i> Loisirs</Link>
       <Link to="/" className="border-bottom"><i className="fa fa-building  mr-2" aria-hidden="true"></i> Entreprises</Link>
      
       <Link to="/" className="border-bottom"><i className="fa fa-bars  mr-2" aria-hidden="true"></i> Autres</Link>
</div>

        </div>
    );
};

export default Sidebar;