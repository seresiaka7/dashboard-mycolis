import React ,{useState} from 'react';
import axios from 'axios';
import Navigation from '../composant/Navigation';
import 'bootstrap/dist/css/bootstrap.min.css';
import SignIn from './SignIn';
const SignUp = () => {

   const [pseudo,setPseudo]=useState("");
    const [email,setEmail]=useState("");
    const [password,setPassword]=useState("");
    const [passControle,setPassControle]=useState("");
    const [error,setError]=useState("");
    const [errorE,setErrorE]=useState("");
    const [errorP,setErrorP]=useState("");
    const [formSubmit,setFormSubmit]=useState(false);
    const handSubmit=(e)=>{
  
        e.preventDefault();
        
               
         const donne={
               pseudo: pseudo,
                email:email,
                password:password
               
             };
             axios.post("http://localhost:5000/api/register", 
             donne)
                 .then((res) =>{
                     console.log(res)

                     if(res.data.errors){

                        setError(res.data.errors.pseudo);
                        setErrorE(res.data.errors.email);
                        setErrorP(res.data.errors.password);
                             }
                             else{
                                 setFormSubmit(true);
                                // window.location="/signin";
                             }
                            }
                        
                 )
                 
                 .catch(e => console.log(e));
        
                
                };
        
     
    return (
 <div>
     { formSubmit ? (
     <>

     <SignIn/>
     <h4 className="text-success"> inscription réussie ,Veuillez vous connecter !!!</h4>
     </>

     ):(

     <>

     
            <Navigation/>

  <div className="container"> 
   <h2>inscription</h2>
      <form className="form-horizontal" onSubmit={handSubmit} >

    <div className="form-group">
      <label className="control-label col-sm-2" htmlFor="pseudo">Pseudo:</label>
      <div className="col-sm-10">
     <input type="text" className="form-control" id="pse" placeholder="Entrer le pseudo" name="pseudo" 
     onChange={(e)=>{setPseudo(e.target.value);}}  />
      </div>
      <div className="errorPseudo text-success"> {error}</div>
      </div>
      
      <div className="form-group">
      <label className="control-label col-sm-2" htmlFor="email">Email:</label>
      <div className="col-sm-10">
        <input type="email" className="form-control" id="em" placeholder="Entrer email" name="email" 
        onChange={(e)=>{setEmail(e.target.value);}} />
      </div>
      <div className="errorEmail text-success"> {errorE}</div>
    </div>
    
    <div className="form-group">
      <label className="control-label col-sm-2" htmlFor="pwd">Password:</label>
      <div className="col-sm-10">          
        <input type="password" className="form-control" id="pwd" placeholder="Entrer password" name="pwd"  
         onChange={(e)=>{setPassword(e.target.value);}}
        /> 
      </div>
      <div className="errorPass text-success"></div>
    </div>
    
    <div className="form-group">
      <label className="control-label col-sm-2" htmlFor="pwd">Confirmer password:</label>
      <div className="col-sm-10">          
        <input type="password" className="form-control" id="pwdcon" placeholder="Entrer password" name="pwd1"
        onChange={(e)=>{setPassControle(e.target.value);}}
         />
      </div>
      <div className="errorPass1 text-success" > {errorP}</div>
    </div>
    

    <div className="form-group">        
      <div className="col-sm-offset-2 col-sm-10">
        <div className="checkbox">
          <label><input type="checkbox" name="remember" id="term"/> j'accepte les termes du contrat </label>
        </div>
        <div className="errorTerm text-success"></div>
      </div>
    </div>
    
    <div className="form-group">        
      <div className="col-sm-offset-2 col-sm-10">
        <input type="submit" className="btn btn-success"  placeholder="Soumettre"/>
      </div>
    </div>
  </form>
  </div>
  </>
  )}
 </div>
 
    )
};

export default SignUp;