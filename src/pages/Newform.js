import React,{useState} from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
const Newform = () => {
   
    const [title,setTitle]=useState("");
    const [description,setDescription]=useState("");
    const [picture,setPicture]=useState("");
    const [telephone,setTelephone]=useState("");
    const [prix,setPrix]=useState("");
    const [ville,setVille]=useState("");
    const [category,setCategory]=useState("");
    const [annonceId,setAnnonceId]=useState("8383528lkl8274242")
const handSubmit=(e)=>{
  
   e.preventDefault();
        const dat={
            annonceId: annonceId,
                title:title,
               description:description,
               picture:picture,
               telephone:telephone,
               prix:prix,
               category:category,
               ville:ville
        };
        
        axios.post("http://localhost:5000/api/annonce/insert", 
        dat)
            .then(res => console.log(res.data) )
            
            .catch(e => console.log(e));
   };
   

const imgUpload= async (e)=>{
    const file=e.target.files[0];
    const   base64= await   convertBase64(file);
 setPicture(base64);
    console.log(base64);
    
};

const convertBase64=(file)=>{
    return  new Promise((resolve,reject)=>{
        const fileReader=new FileReader();
        fileReader.readAsDataURL(file);
        fileReader.onload=()=>{
            resolve(fileReader.result);
        };
        fileReader.onerror=(error)=>{
            reject(error);
        };

    } );
};
  
       
       
     
    

    return (
        <div>
        <div className="container">
            <h1 className="text-center">Créer une annonce </h1>
            <div className="form">   
            <form action="" method="post" className="form-horizontal">
            <div className="form-group">
            <label className="control-label col-sm-2" htmlFor="">annonceid</label>
                <div className="col-sm-10"> <input className="form-control" type="text" onChange={(e)=>{setAnnonceId(e.target.value);}}/>
                </div>
                </div>
                <div className="form-group">
                <label  className="control-label col-sm-2" htmlFor="">titre</label>
                <div className="col-sm-10">
                <input className="form-control"type="text" onChange={(e)=>{setTitle(e.target.value);}}/>
                </div>
                </div>
                <div className="form-group">
                <label  className="control-label col-sm-2" htmlFor=""> description</label>
                <div className="col-sm-10">
                <textarea className="form-control" name="desc" id="" cols="30" rows="10" onChange={(e)=>{setDescription(e.target.value);}}></textarea>
               </div>
               </div>
                <div className="form-group">
                <label className="control-label col-sm-2" htmlFor=""> Ajouter une image</label>
                <div className="col-sm-10">
                <input type="file"  onChange={(e)=>{imgUpload(e);}}/>
                </div> 
                </div>
                <div className="form-group">
                <label className="control-label col-sm-2"  htmlFor=""> Prix</label>
                <div className="col-sm-10">
               <input className="form-control" type="number" onChange={(e)=>{setPrix(e.target.value);}}/>
              
              </div>
              </div>
               <div className="form-group">
               <label  className="control-label col-sm-2" htmlFor="">telephone</label>
               <div className="col-sm-10">
               <input className="form-control" type="tel" name="" id="te" onChange={(e)=>{setTelephone(e.target.value);}}/>
              
              </div>
              </div>
               <div className="form-group">
               <label className="control-label col-sm-2" htmlFor="">ville</label>
               <div className="col-sm-10">
               <input className="form-control" type="text" name="" id="e" onChange={(e)=>{setVille(e.target.value);console.log(ville);}}/>
               </div>
               </div>
         <div className="form-group">
               <label className="control-label col-sm-2" htmlFor="">category</label>
               <div className="col-sm-10">
               <input className="form-control" type="text" name="" id="t" onChange={(e)=>{setCategory(e.target.value);}}/>
              
                </div>
               </div>
         <div className="form-group">
            <div className="col-sm-offset-2 col-sm-10">
                <button type="submit" onClick={handSubmit} className="btn btn-danger">Enregistrer</button>
             </div>
         </div>
         </form>
        </div>
        </div>
        </div>

    );
};

export default Newform;