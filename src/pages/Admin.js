
import './admin.css';
import { NavLink as Link, } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import React, {useState,useEffect } from 'react';
import { auth,db } from '../Firebase';
import { Button,Modal } from 'react-bootstrap';
import moment from "moment";
const Admin = () => {

    const [annonce,setAnnonce]=useState([]);
    const [search,setSearch]=useState("");
  
   
    const [image,setImage]= useState("https://kerala.nic.in/wp-content/themes/sdo-theme/images/default/image-gallery.jpg");
    const [taille,setTaille]=useState("1 kg - 5 kg");
    const [paysDep,setPaysDep]=useState("Allemagne");
    const [paysAr,setPaysAr]=useState("Allemagne");
    const [prix,setPrix]=useState("");
    const [desc,setDesc]=useState("");
    const [titre,setTitre]=useState("");
    const [date,setDate]=useState(moment());



    
   const [imageU, setImageU] = useState("");
   const [tailleU,setTailleU]=useState("");
   const [paysDepU,setPaysDepU]=useState("");
   const [paysArU,setPaysArU]=useState("");
   const [prixU,setPrixU]=useState("");
   const [descU,setDescU]=useState("");
   const [titreU,setTitreU]=useState("");
   const [dateU,setDateU]=useState("");
   const [IdAnnonce,setIdAnnonce]=useState("");
   const [Iduser,setIduser]=useState("");
    const onAnnonce= async ()=>{
  
     
    
        await   db.collection("annonces")
          .add({
                   Iduser:"12325565", 
                   date_depart:date,
                   ville_depart:paysDep,
                   ville_arrivee:paysAr,
                   taille:taille,
                   image:image,
                   titre:titre,
                   description:desc,
                   prix:prix,
               })
               
               .then((res)=> {  
                 
                handleClose();
                 //navigation.navigate('ListAnnonce');
                })  
               .catch((error)=>
               console.log(error)
               ); 
               
               // 
               
    };





const imgUpload= async (e)=>{
    const file=e.target.files[0];
    const   base64= await   convertBase64(file);
 setImage(base64);
    console.log(base64);
    
};

const imgUploadU= async (e)=>{
  const file=e.target.files[0];
  const   base64= await   convertBase64(file);
setImageU(base64);
  console.log(base64);
  
};


const convertBase64=(file)=>{
    return  new Promise((resolve,reject)=>{
        const fileReader=new FileReader();
        fileReader.readAsDataURL(file);
        fileReader.onload=()=>{
            resolve(fileReader.result);
        };
        fileReader.onerror=(error)=>{
            reject(error);
        };

    } );
};
  
       




const [show, setShow] = useState(false);

const handleClose = () => setShow(false);


const handleShow = (data) =>{
   
  setShow(true);
  setDateU(data.date_depart);
  setIdAnnonce(data.id);
  setIduser(data.Iduser);
  setDateU(data.date_depart);
  setPaysDepU(data.ville_depart);
  setPaysArU(data.ville_arrivee);
  setTailleU(data.taille);
  setPrixU(data.prix);
  setDescU(data.description);
  setTitreU(data.titre);
  setImageU(data.image)
};

const handleUpdate = async () =>{
 
    
  await db.collection("annonces").doc(IdAnnonce).set({
    

    Iduser:Iduser, 
    date_depart:dateU,
    ville_depart:paysDepU,
    ville_arrivee:paysArU,
    taille:tailleU,
    image:imageU,
    titre:titreU,
    description:descU,
    prix:prixU,


   
                
  }).then(() => {
      handleClose();
      alert("Document successfully Update!");
      getAnnonce();
  }).catch((error) => {
      console.error("Error  document: ", error);
  });

};










   const getAnnonce= () =>{
       try {
        db.collection("annonces").onSnapshot((querySnapshot) => {
          const Trajet = [];
          querySnapshot.forEach((doc) => {
            const {   date_depart,  image, ville_depart,ville_arrivee,titre ,Iduser,description,taille} = doc.data();
            Trajet.push({  
              id: doc.id,
              date_depart,  image, ville_depart,ville_arrivee,titre,Iduser,description,taille
            });
          });
          setAnnonce(Trajet);
         // setDate(Trajet.date_depart);
         // setIsLoading(false);
        });
       } catch (error) {
          console.log(error); 
       }
       
      };

 
 

  useEffect(()=>{

    getAnnonce();
    
  },[]);


  const handDelete = async (id)=> {
        
     await db.collection("annonces").doc(id).delete().then(() => {
      console.log("Document successfully deleted!");
      getAnnonce();
  }).catch((error) => {
      console.error("Error removing document: ", error);
  });
    
  /*  if(res){
       getAnnonce();
    }*/
   
  };





  const tab= annonce.map((val,i)=>{
    return(
     
    <tr key={i} >
       <td className="text-bold">{val.titre}</td>
  <td className="text-bold">{val.ville_depart}</td>
  <td className="text-bold">{val.ville_arrivee}</td>
 
  <td  className=" ml-4">
  <button className="btn btn-danger"  onClick={()=>handDelete(val.id)}><i className="fa fa-trash-o text-gray" aria-hidden="true"></i> Delete</button>
 

  <Button variant="warning ml-3"    onClick={()=>{handleShow(val)}}>
       Edit
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <div  className="ml-5">
        <Modal.Title > Update Announce</Modal.Title>
        </div>
        </Modal.Header>
        <Modal.Body>
       

        <div className="form">   
            <form action="" method="post" className="form-horizontal">
           
                <div className="form-group">
                <label  className="control-label col-sm-6" htmlFor="">Titre de l'annonce</label>
                <div className="col-sm-10">
                <input className="form-control"type="text" placeholder="Entrez le titre !"  value={titreU} onChange={(e)=>{setTitreU(e.target.value);}}/>
                </div>
                </div>

                <div class="form-group">
                 <label for="exampleFormControlSelect1">Pays de depart</label>
                  <select class="form-control" placeholder=" select" id="exampleFormControlSelect1"
                   onChange={(e)=>{setPaysDepU(e.target.value);}} value={paysDepU}
                  >
                
                 <option value="Allemagne">Allemagne</option>
                  <option  value="France"> France</option>
                  <option  value="Togo">Togo</option>
                  <option  value="Tunise"> Tunise</option>
                
                   </select>
                </div>

                 
                <div class="form-group">
                 <label for="exampleFormControlSelect1">Pays d'arrivé</label>
                  <select class="form-control"  id="exampleFormControlSelect"
                   onChange={(e)=>{setPaysArU(e.target.value);}} value={paysArU}
                  >
                  <option value="Allemagne">Allemagne</option>
                  <option  value="France"> France</option>
                  <option  value="Togo">Togo</option>
                  <option  value="Tunise"> Tunise</option>
                 
                
                   </select>
                </div>

                


                <div className="form-group">
                <label className="control-label col-sm-6" htmlFor=""> Ajouter une image</label>
                <div className="col-sm-10">
                <input type="file"  onChange={(e)=>{imgUploadU(e);}} />
                </div> 
                <img className="card-img-top" src={imageU} alt=" image annonce" width="70px" height="200px"/>
                </div>
                

                <div class="form-group">
                 <label for="exampleFormControlSelect1">Taille</label>
                  <select class="form-control"  id="exampleFo"
                   onChange={(e)=>{setTailleU(e.target.value);}} value={tailleU}
                  >
                  <option value="1 kg - 5 kg">1 kg - 5 kg</option>
                  <option  value="5 kg - 10 kg">5 kg - 10 kg</option>
                  <option  value="10 kg - 15 kg">10 kg - 15 kg</option>
                  <option  value="plus ..."> plus ...</option>
                
                  
                   </select>
                </div>

                    
                <div className="form-group">
                <label className="control-label col-sm-2"  htmlFor=""> Prix</label>
                <div className="col-sm-10">
                <input className="form-control" type="number" value={prixU} onChange={(e)=>{setPrixU(e.target.value);}}/>
                 </div>
                </div>


                <div className="form-group">
                <label className="control-label col-sm-2"  htmlFor=""> Date </label>
                <div className="col-sm-10">
                <input className="form-control " type="date" value={dateU} onChange={(e)=>{setDateU(e.target.value);}}/>
                 </div>
                </div>

     


                <div className="form-group">
                <label  className="control-label col-sm-6" htmlFor=""> Description</label>
                <div className="col-sm-10">
                <textarea className="form-control" name="desc" id="" cols="30" rows="10" value={descU} onChange={(e)=>{setDescU(e.target.value);}}></textarea>
               </div>
               </div>
               
                
               
               
         </form>
        </div>




        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Cancel
          </Button>
          <Button variant="primary" onClick={handleUpdate}>
            Valider
          </Button>
        </Modal.Footer>
      </Modal>



    </td>
    </tr>
    )
   });

    return (

        
        <div>
            <div className="row">
                <div className="col-md-2">
       <div className="sidebarse">

          <h4 className="border-bottom mt-3 p-1"><i className="fa fa-tachometer mr-2" aria-hidden="true"></i>Dashboard</h4>
    
       <Link to="admin" className="border-bottom mt-3"><i className="fa fa-cart-arrow-down mr-2" aria-hidden="true"></i>Annonce</Link>
       <Link to="contact" className="border-bottom mt-3"><i className="fa fa-user  mr-2" aria-hidden="true"></i> Users</Link>
       <Link to="trajet" className="border-bottom mt-3"><i className="fa fa-fw fa-envelope  mr-2"></i>Trajet</Link>
       
       <Link to="/admin" className="border-bottom mt-3"><i className="fa fa-fw fa-home  mr-2"></i> Charts</Link>
       <Link to="/admin" className="border-bottom mt-3"><i className="fa fa-shopping-cart  mr-2" aria-hidden="true"></i> Setting</Link>
       <Link to="/admin" className="border-bottom mt-3"><i className="fa fa-building  mr-2" aria-hidden="true"></i> Pages</Link>
      
       <Link to="/admin" className="border-bottom mt-3"><i className="fa fa-bars  mr-2" aria-hidden="true"></i> Autres</Link>
       
         </div>
    </div>
    <div className="col-md-10">
    <div className="main">
    
  
    <Button variant="primary"  className="mt-3 text-bold" onClick={handleShow}>
        Ajouter Une Annonce
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <div  className="ml-5">
        <Modal.Title > Add Announce</Modal.Title>
        </div>
        </Modal.Header>
        <Modal.Body>
       

        <div className="form">   
            <form action="" method="post" className="form-horizontal">
           
                <div className="form-group">
                <label  className="control-label col-sm-6" htmlFor="">Titre de l'annonce</label>
                <div className="col-sm-10">
                <input className="form-control"type="text" placeholder="Entrez le titre !" onChange={(e)=>{setTitre(e.target.value);}}/>
                </div>
                </div>

                 <div class="form-group">
                 <label for="exampleFormControlSelect1">Pays de depart</label>
                  <select class="form-control" placeholder=" select" id="exampleFormControlSelect1"
                   onChange={(e)=>{setPaysDep(e.target.value);}}
                  >
                
                <option value="Allemagne">Allemagne</option>
                  <option  value="France"> France</option>
                  <option  value="Togo"> Togo</option>
                  <option  value="Tunise"> Tunise</option>
                 
                
                   </select>
                </div>

                 
                <div class="form-group">
                 <label for="exampleFormControlSelect1">Pays d'arrivé</label>
                  <select class="form-control"  id="exampleFormControlSelect"
                   onChange={(e)=>{setPaysAr(e.target.value);}}
                  >
                  <option  value="Allemagne">Allemagne</option>
                  <option  value="France"> France</option>
                  <option  value="Togo"> Togo</option>
                  <option  value="Tunise"> Tunise</option>
                  
                   </select>
                </div>

                


                <div className="form-group">
                <label className="control-label col-sm-6" htmlFor=""> Ajouter une image</label>
                <div className="col-sm-10">
                <input type="file"  onChange={(e)=>{imgUpload(e);}}/>
                </div> 
                <img className="card-img-top" src={image} alt="Card image cap" width="70px" height="200px"/>
                </div>
                

                <div class="form-group">
                 <label for="exampleFormControlSelect1">Taille</label>
                  <select class="form-control"  id="exampleFo"
                   onChange={(e)=>{setTaille(e.target.value);}}
                  >
                  <option value="1 kg - 5 kg">1 kg - 5 kg</option>
                  <option  value="5 kg - 10 kg">5 kg - 10 kg</option>
                  <option  value="10 kg - 15 kg">10 kg - 15 kg</option>
                  <option  value="plus ..."> plus ...</option>
                
                  
                   </select>
                </div>

                    
                <div className="form-group">
                <label className="control-label col-sm-2"  htmlFor=""> Prix</label>
                <div className="col-sm-10">
                <input className="form-control" type="number" onChange={(e)=>{setPrix(e.target.value);}}/>
                 </div>
                </div>
 

                <div className="form-group">
                <label className="control-label col-sm-2"  htmlFor=""> Date </label>
                <div className="col-sm-10">
                <input className="form-control " type="date" onChange={(e)=>{setDate(e.target.value);}}/>
                 </div>
                </div>

     
                <div className="form-group">
                <label  className="control-label col-sm-6" htmlFor=""> Description</label>
                <div className="col-sm-10">
                <textarea className="form-control" name="desc" id="" cols="30" rows="10" onChange={(e)=>{setDesc(e.target.value);}}></textarea>
               </div>
               </div>
               
                
                </form>
        </div>

        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Cancel
          </Button>
          <Button variant="primary" onClick={onAnnonce}>
            Valider
          </Button>
        </Modal.Footer>
      </Modal>



  <h2>Liste des Annonces</h2>

  
  <table className="table table-striped text-bold">
    <thead>
    <tr>
      
      <th>Titre</th>
      <th>Pays de depart</th>
      <th>Pays d'arrivé</th>
      <th></th>
      </tr>
    </thead>
   <tbody>

    {tab}
      </tbody>
      </table>
     </div>
      </div>

        </div>





        </div>
    );
};

export default Admin;