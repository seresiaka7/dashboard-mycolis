//import * as firebase from 'firebase';
import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
/*const firebaseConfig = {
    apiKey: "AIzaSyDpyC-jQDEM2lFKs7aczsrcLDYm2G3SIp0",
    authDomain: "mycolis-1fe33.firebaseapp.com",
    projectId: "mycolis-1fe33",
    storageBucket: "mycolis-1fe33.appspot.com",
    messagingSenderId: "324803132855",
    appId: "1:324803132855:web:b18fb4b981625677d8fc32",
    measurementId: "G-EP5RL76T7Q"
  };*/

  /*const  firebaseConfig = {
    apiKey: "AIzaSyALBgpLAbBYHqp8GxSR7XQRD00tI53re6o",
    authDomain: "mycolis7.firebaseapp.com",
    projectId: "mycolis7",
    storageBucket: "mycolis7.appspot.com",
    messagingSenderId: "189477505867",
    appId: "1:189477505867:web:b569449057d925fe2c36ce",
    measurementId: "G-6KRBS41Z7M"
  };

*/
  const firebaseConfig = {
    apiKey: "AIzaSyC6wvcNcXkSlDIdOisdhA4rSoxQFvk0vOU",
    authDomain: "mycolis-pfe-f6cde.firebaseapp.com",
    projectId: "mycolis-pfe-f6cde",
    storageBucket: "mycolis-pfe-f6cde.appspot.com",
    messagingSenderId: "695618156786",
    appId: "1:695618156786:web:07827d923412bfd03ab86b",
    measurementId: "G-6599K87H1V"
  };






  let app;
  if(firebase.apps.length === 0){
      app=firebase.initializeApp(firebaseConfig);

  }
  else{
      app=firebase.app();
  }
  const db=app.firestore();
  const auth=firebase.auth();

  export {db,auth};  


  /*

 Google Certificate Fingerprint:     BD:E0:A6:86:DC:C9:71:34:7D:F9:20:39:EF:BC:C7:AF:CC:CD:42:8D
Google Certificate Hash (SHA-1):    BDE0A686DCC971347DF92039EFBCC7AFCCCD428D
Google Certificate Hash (SHA-256):  6FB04E6A1819A6111A367E8F5F4B55C12E03843CA2DC3F0AC8A96ABA8E834384
Facebook Key Hash:                  veCmhtzJcTR9+SA577zHr8zNQo0=
ID_client :971262823188-jkhnflv2kbrgn45uvk2rpu7eqo68t4u5.apps.googleusercontent.com
// 
},
      "package": "com.sereline.mycolis"
      "googleServicesFile": "./google-services.json"
      "package": "host.exp.exponent"

       "web": {
      "favicon": "./assets/favicon.png"
    }
  */