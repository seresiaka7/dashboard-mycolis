import React from 'react';
import {
  Nav,
  NavLink,
  Bars,
  NavMenu,
  NavBtn,
  NavBtnLink
} from './StyleNavbar.js';


/*const logout=()=>{
  const removeCookie=(key)=>{
      if(window !== "undefined"){
          cookie.remove(key,{expires:1});
      }
  };
   
const Logout=  () =>{
 
   axios.get("http://localhost:5000/api/logout")
   .then(()=>  removeCookie("jwt"))
   .catch (error)=>  console.log(error)); 
   window.location="/";
};
*/
const Navigation = () => {
  return (
    <>
      <Nav>
        <NavLink to='/' style={{textDecoration:"none"}}>
          <h1>AFRICASOW</h1>
        </NavLink>
        <Bars />
        <NavMenu>
          <NavLink exact to='/' className="activeStyle" style={{textDecoration:"none"}}>
            Home
          </NavLink>
          <NavLink to='/service' className="activeStyle" style={{textDecoration:"none"}}>
            Services
          </NavLink>
          <NavLink to='/contact' className="activeStyle" style={{textDecoration:"none"}}>
            Contact 
          </NavLink>
          <NavLink to='/signup' className="activeStyle" style={{textDecoration:"none"}}>
           s'inscrire
          </NavLink>
          {/* Second Nav */}
          {/* <NavBtnLink to='/sign-in'>Sign In</NavBtnLink> */}
        </NavMenu>
        <NavBtn>
          <NavBtnLink to='/signin' style={{textDecoration:"none"}}>Se connecter</NavBtnLink>
        </NavBtn>
      </Nav>
    </>
  );
};

export default Navigation;