
import './App.css';
import {BrowserRouter as Router, Route,Switch} from 'react-router-dom';
import Home from './pages/Home.js';
import Service from './pages/Service.js';
import Contact from './pages/Contact.js';
//import Edit from './pages/Edit.js';
import Detail from './pages/Detail';
import SignUp from './pages/SignUp.js';
import SignIn from './pages/SignIn.js';
import Vehicule from './pages/Vehicule.js';
import Admin from './pages/Admin.js';
import NotFound from './pages/NotFound.js';
import Trajet from './pages/Trajet';

function App() {
  return (
    <>
     <Router>
       <Switch>
      
         <Route  path="/" exact component={Home}/>
         <Route  path="/service"  exact component={Service}/>
         <Route   path="/service/:id" component={Detail}/> 
         <Route   path="/contact"  component={Contact}/>
         <Route   path="/trajet"  component={Trajet}/>
         <Route   path="/signup"  component={SignUp}/>
         <Route   path="/signin"  component={SignIn}/>
         <Route   path="/vehicule"  component={Vehicule}/>
         <Route   path="/admin"  component={Admin}/>
          <Route  component={NotFound}/>
         
       </Switch>
     </Router>
     
    </>
  );
}

export default App;

 